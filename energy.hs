import Data.Function (on)
import Data.List (sortBy)

-- Unit cost (pence per kWh)
unit = 43.40

-- Common durations
minute = 60
hour = 60 * minute
evening = 6 * hour
day = 24 * hour
work = 8 * hour
weeksPerMonth = 5
daysPerMonth = 31

-- Appliances around my house
appliances = [
        -- detail, power (Wh), duration (seconds), monthly count
        ("Washing machine", 824, hour, 2 * weeksPerMonth),
        ("Dehumidifier", 163.3, 6 * hour, 2 * weeksPerMonth),
        ("Kettle (0.5 litres)", 2858, 73, 5 * daysPerMonth),
        ("Thermomix as a kettle (0.5 litres)", 0, 4 * minute, 0),
        ("Actifry (cooking bacon)", 1370, 12 * minute, 2 * weeksPerMonth),
        ("Actifry (making toast)", 1370, 5 * minute, 2 * weeksPerMonth),
        ("Toaster", 1000, 4 * minute, 0),        
        ("Energy saver lightbulb", 4.7, evening, daysPerMonth),
        ("WiFi extender (both ends)", 2.6 + 1.4, day, daysPerMonth),
        ("All 6 kitchen lights", 28.2, evening, daysPerMonth),
        ("Phone charger (full charge)", 13, 2 * hour, daysPerMonth),
        ("Laptop (idle)", 9, work, 0),
        ("Laptop (8 cores busy)", 49, work, 5 * weeksPerMonth),
        ("24\" monitor", 10.5, work, 5 * weeksPerMonth),
        ("Dishwasher", 1300, 51 * minute, daysPerMonth),
        ("Microwave", 700, 3 * minute, daysPerMonth),
        ("Gas combi boiler", 60, 4 * hour, daysPerMonth),
        ("Smoke alarm", 9, day, 3 * daysPerMonth),
        ("Extractor fan", 4.8, 20 * minute, 5 * 2 * daysPerMonth),
        ("Electric shower", 10500, 10 * minute, 0),
        ("Nest thermostat", 1.34, day, daysPerMonth),
        ("Virgin router", 10.8, day, daysPerMonth),
        ("Fridge/freezer", 99.4, day, daysPerMonth),
        ("Smart socket", 0.5, day, 0),
        ("Socket timer", 0.8, day, 0),
        ("Sony 55\" TV (idle)", 0, day - evening, daysPerMonth),
        ("Sony 55\" TV (viewing)", 100, evening, daysPerMonth),
        ("NAS", 510 / 24, day, daysPerMonth),
        ("Sonos One (idle)", 3.5, day - evening, daysPerMonth),
        ("Sonos One (playing quietly)", 4.9, evening, daysPerMonth),
        ("Google One", 1.4, day, daysPerMonth),
        ("Dryer", 1405, 2 * hour, 2 * weeksPerMonth),
        ("Bank of 10 monitors (work)", 108, work, daysPerMonth),
        ("Daily standing charge", 948 / 24, day, daysPerMonth)
        ]

-- Calculate the cost of running a device for a certain duration
calculateCost power duration = unit * (power / 1000) * duration / hour
-- calculateMonthlyCost power duration = unit * (power / 1000) * duration / hour

-- Calculate cost for each appliance and sort by most costly
summary = [(device, power, duration, calculateCost power duration, count * calculateCost power duration / 100.0) | (device, power, duration, count) <- appliances]

getCost (_, _, _, _, cost) = cost
sorted_summary = reverse $ sortBy (compare `on` getCost) summary

-- Create row of a markdown table
tabulate device power duration cost monthly =
    "| " ++ device ++ " | " ++ show power ++ " | " ++ show(round cost) ++ " | " ++ show(round monthly) ++ " |\n"

-- Pretty print the results
pretty = [tabulate device power duration cost count | (device, power, duration, cost, count) <- sorted_summary]

main = do
    putStr "| Appliance | Average Wh | Cost per use (p) | Monthly (GBP) |\n"
    putStr "| --- | --- | --- | --- |\n"
    putStr $ concat pretty
