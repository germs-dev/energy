# Electricity usage by appliance
"Every penny counts"... or does it?!

Charges for Brighton on 12/01/22. See [Octopus energy rates](https://octopus.energy/go/rates/) for your postcode.

- Unit rate (04:30 - 00:30): 43.40p / kWh
- Unit rate (00:30 - 04:30): 12.00p / kWh
- Standing Charge: 41.13p / day

If you make sure to turn off your phone charger off at the plug when you're not using it, you won't notice _any_ difference in your bills: focus your attention on the big problems first. E.g., if your oven is the worst offender in your house, how can you avoid using it? Is an air fryer cheaper to run? If you can't measure something then you don't know if you're improving things. It's tempting to just look at the big hitters, but I've factored in the typical duration of usage for each appliance. For instance, a kettle consumes 3kW per hour but it only runs for a few minutes. Interestingly, the standing charge appears to be equivalent to running a 40W lightbulb for the whole day.

The ability to time appliances to turn off through the night using timer sockets is very interesting, but you do need to factor in the power usage of the timer socket itself. Surprisingly, the old mechanical version consumes 0.8W regardless of whether the output is live; the wifi enabled socket, however, only uses 0.6W when on and nothing when the output is off.

The fourth column is my monthly usage, for instance I don't have an electric shower but the numbers for a single use are of interest.

